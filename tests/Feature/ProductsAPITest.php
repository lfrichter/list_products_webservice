<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProductsAPITest extends TestCase
{
    public function testIndexStatus()
    {
        $response = $this->get(route('products.index'));

        $response->assertStatus(200);
    }

    public function testIndexSanitized()
    {
        $products = $this->get(route('products.index'));
        $data = (array) $products->getData();
        $message = 'String has a HTML code or any other character not valid';

        foreach ($data as $key => $item) {
            $response = $this->get(route('products.show', $key));

            $this->assertFalse($this->isHtml($item), $message);
            $this->assertStringNotContainsString('', $item);
            $this->assertStringNotContainsString('\"', $item);
        }
    }

    public function testShowStatus()
    {
        $products = $this->get(route('products.index'));
        $data = (array) $products->getData();

        foreach ($data as $key => $item) {
            $response = $this->get(route('products.show', $key));
            $response->assertStatus(200);
        }
    }

    public function testShowSanitized()
    {
        $products = $this->get(route('products.index'));
        $data = (array) $products->getData();

        $message = 'String has a HTML code or any other character not valid';

        foreach ($data as $key => $item) {
            $response = $this->get(route('products.show', $key));
            $product = $response->getData();

            $this->assertFalse($this->isHtml($product->name), $message);
            $this->assertStringNotContainsString('', $product->name);
            $this->assertStringNotContainsString('\"', $product->name);

            $this->assertFalse($this->isHtml($product->description), $message);
            $this->assertStringNotContainsString('', $product->description);
            $this->assertStringNotContainsString('\"', $product->description);

            if (isset($product->type)) {
                $this->assertFalse($this->isHtml($product->type), $message);
                $this->assertStringNotContainsString('', $product->type);
                $this->assertStringNotContainsString('\"', $product->type);
            }

            if (isset($product->suppliers)) {
                foreach ($product->suppliers as $supply) {
                    $this->assertFalse($this->isHtml($supply), $message);
                    $this->assertStringNotContainsString('', $supply);
                    $this->assertStringNotContainsString('\"', $supply);
                    $this->assertStringNotContainsString('78a1bc9e567fa197bb3', $supply);
                    $this->assertStringNotContainsString('&#39;', $supply);
                }
            }
        }
    }

    public function isHtml($string)
    {
        return preg_match('/<[^<]+>/', $string, $m) != 0;
    }
}
