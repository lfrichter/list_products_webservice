## Products
[![Composer](https://img.shields.io/badge/Composer-2.0.5-885630.svg?logo=composer&logoColor=white&style=for-the-badge)](https://getcomposer.org/)                                                                                                                                                                                                                                     [![git](https://img.shields.io/badge/Git-2.25.1-F05032.svg?logo=git&style=for-the-badge&logoColor=white)](https://git-scm.com/downloads)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         [![php](https://img.shields.io/badge/php-7.2.34-777BB4.svg?logo=php&logoColor=white&style=for-the-badge)](http://php.net/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                   [ ![Laravel](https://img.shields.io/badge/Laravel-7.30.4-E74430.svg?logo=laravel&logoColor=white&style=for-the-badge)](https://laravel.com/)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         [![npm](https://img.shields.io/badge/NPM-7.5.1-CB3837.svg?logo=npm&style=for-the-badge&logoColor=white)](https://www.npmjs.com)                                                                                                                                                                                                                                                                                                                                  [![Node.js](https://img.shields.io/badge/Node-15.8.0-339933.svg?logo=node.js&style=for-the-badge&logoColor=white)](https://nodejs.org/en/)

Coding standard used is **PSR-2**       

### Instalation
1. `composer install`
2. `npm install && npm run dev`

### Running
- `php artisan serve`
      
### Endpoints
- Index `/api/products`                                                                                                                                                                                           
- Show `/api/products/{product_id}`

### Dependencies
- [Waavi Sanitizer](https://github.com/Waavi/Sanitizer)

### Tests
Run tests with `.\vendor\bin\phpunit`

### View interface
- Vue.js
- Route:  [/](http://localhost/)

#### Screenshot
![products](https://i.imgur.com/MpQ9XQz.png)


