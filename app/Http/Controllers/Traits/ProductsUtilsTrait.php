<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Facades\Http;

trait ProductsUtilsTrait
{
    protected function requestProduct($endpoint = '', $root_name = '')
    {
        $data = '';

        do {
            $response = Http::get(env('WEBSERVICE_PRODUCTS').$endpoint);
            $data = $response->json();
        } while (isset($data['error']));

        if (!empty($root_name)) {
            $data = $data[$root_name];
        }

        return $data;
    }
}
