<?php

namespace App\Sanitizer;

use Waavi\Sanitizer\Sanitizer;

class ProductSanitizer
{
    private $products;
    private $product;

    public function index(array $products): array
    {
        $filters = [
            'combgap' => 'trim|escape|strip_tags',
            'smart' => 'trim|escape|strip_tags',
            'annualtravel' => 'trim|escape|strip_tags',
            'singletravel' => 'trim|escape|strip_tags',
            'buildcont' => 'trim|escape|strip_tags',
            'income' => 'trim|escape|strip_tags',
            'car' => 'trim|escape|strip_tags',
        ];

        $sanitizer = new Sanitizer($products, $filters);
        $this->products = $sanitizer->sanitize();

        return $this->products;
    }

    public function show(array $product): array
    {
        $customFilters = [
            'remove_strings' => RemoveStringsFilter::class,
        ];

        $customFiltersSupply = [
            'remove_strings' => RemoveStringsFilter::class,
        ];

        $filters = [
            'name' => 'trim|escape|strip_tags',
            'description' => 'trim|escape|strip_tags|remove_strings:&#34;,',
            'type' => 'trim|escape|strip_tags',
        ];

        if (isset($product['suppliers'])) {
            $filter_Supplier = [0 => 'trim|escape|strip_tags|remove_strings:78a1bc9e567fa197bb3,&#39;'];
            $product_suppliers = $product['suppliers'];

            foreach ($product_suppliers as $key => $suplier) {
                $arr = new Sanitizer([$suplier], $filter_Supplier, $customFiltersSupply);
                $product_suppliers[$key] = $arr->sanitize()[0];
            }

            $product['suppliers'] = $product_suppliers;
        }

        $sanitizer = new Sanitizer($product, $filters, $customFilters);
        $this->product = $sanitizer->sanitize();

        return $this->product;
    }
}
