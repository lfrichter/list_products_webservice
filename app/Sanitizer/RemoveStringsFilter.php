<?php

namespace App\Sanitizer;

use Waavi\Sanitizer\Contracts\Filter;

class RemoveStringsFilter implements Filter
{
    public function apply($value, $options = [])
    {
        return str_replace($options, '', $value);
    }
}
